toc = [
  {label:"BASIC",children:
  [
    {label:"quickref",path:"quickref.html"},
    {label:"sponsor",path:"sponsor.html"}
  ]},

  {label:"USER MANUAL",children:
  [
    {label:"usr_toc",path:"usr_toc.html"}
  ]},

  {label:"Getting Started",children:
  [
    {label:"usr_01",path:"usr_01.html"},
    {label:"usr_02",path:"usr_02.html"},
    {label:"usr_03",path:"usr_03.html"},
    {label:"usr_04",path:"usr_04.html"},
    {label:"usr_05",path:"usr_05.html"},
    {label:"usr_06",path:"usr_06.html"},
    {label:"usr_07",path:"usr_07.html"},
    {label:"usr_08",path:"usr_08.html"},
    {label:"usr_09",path:"usr_09.html"},
    {label:"usr_10",path:"usr_10.html"},
    {label:"usr_11",path:"usr_11.html"},
    {label:"usr_12",path:"usr_12.html"}
  ]},

  {label:"Editing Effectively",children:
  [
    {label:"usr_20",path:"usr_20.html"},
    {label:"usr_21",path:"usr_21.html"},
    {label:"usr_22",path:"usr_22.html"},
    {label:"usr_23",path:"usr_23.html"},
    {label:"usr_24",path:"usr_24.html"},
    {label:"usr_25",path:"usr_25.html"},
    {label:"usr_26",path:"usr_26.html"},
    {label:"usr_27",path:"usr_27.html"},
    {label:"usr_28",path:"usr_28.html"},
    {label:"usr_29",path:"usr_29.html"},
    {label:"usr_30",path:"usr_30.html"},
    {label:"usr_31",path:"usr_31.html"},
    {label:"usr_32",path:"usr_32.html"}
  ]},

  {label:"Tuning Vim",children:
  [
    {label:"usr_40",path:"usr_40.html"},
    {label:"usr_41",path:"usr_41.html"},
    {label:"usr_42",path:"usr_42.html"},
    {label:"usr_43",path:"usr_43.html"},
    {label:"usr_44",path:"usr_44.html"},
    {label:"usr_45",path:"usr_45.html"}
  ]},

  {label:"Making Vim Run",children:
  [
    {label:"usr_90",path:"usr_90.html"}
  ]},

  {label:"General subjects",children:
  [
    {label:"intro",path:"intro.html"},
    {label:"help",path:"index.html"},
    {label:"helphelp",path:"helphelp.html"},
    {label:"index",path:"vimindex.html"},
    {label:"tags",path:"tags.html"},
    {label:"howto",path:"howto.html"},
    {label:"tips",path:"tips.html"},
    {label:"message",path:"message.html"},
    {label:"quotes",path:"quotes.html"},
    {label:"todo",path:"todo.html"},
    {label:"debug",path:"debug.html"},
    {label:"develop",path:"develop.html"},
    {label:"uganda",path:"uganda.html"}
  ]},

  {label:"Basic editing",children:
  [
    {label:"starting",path:"starting.html"},
    {label:"editing",path:"editing.html"},
    {label:"motion",path:"motion.html"},
    {label:"scroll",path:"scroll.html"},
    {label:"insert",path:"insert.html"},
    {label:"change",path:"change.html"},
    {label:"indent",path:"indent.html"},
    {label:"undo",path:"undo.html"},
    {label:"repeat",path:"repeat.html"},
    {label:"visual",path:"visual.html"},
    {label:"various",path:"various.html"},
    {label:"recover",path:"recover.html"}
  ]},

  {label:"Advanced editing",children:
  [
    {label:"cmdline",path:"cmdline.html"},
    {label:"options",path:"options.html"},
    {label:"pattern",path:"pattern.html"},
    {label:"map",path:"map.html"},
    {label:"tagsrch",path:"tagsrch.html"},
    {label:"quickfix",path:"quickfix.html"},
    {label:"windows",path:"windows.html"},
    {label:"tabpage",path:"tabpage.html"},
    {label:"syntax",path:"syntax.html"},
    {label:"spell",path:"spell.html"},
    {label:"diff",path:"diff.html"},
    {label:"autocmd",path:"autocmd.html"},
    {label:"filetype",path:"filetype.html"},
    {label:"eval",path:"eval.html"},
    {label:"channel",path:"channel.html"},
    {label:"fold",path:"fold.html"}
  ]},

  {label:"Special issues",children:
  [
    {label:"print",path:"print.html"},
    {label:"remote",path:"remote.html"},
    {label:"term",path:"term.html"},
    {label:"digraph",path:"digraph.html"},
    {label:"mbyte",path:"mbyte.html"},
    {label:"mlang",path:"mlang.html"},
    {label:"arabic",path:"arabic.html"},
    {label:"farsi",path:"farsi.html"},
    {label:"hebrew",path:"hebrew.html"},
    {label:"russian",path:"russian.html"},
    {label:"ft_ada",path:"ft_ada.html"},
    {label:"ft_sql",path:"ft_sql.html"},
    {label:"hangulin",path:"hangulin.html"},
    {label:"rileft",path:"rileft.html"}
  ]},

  {label:"GUI",children:
  [
    {label:"gui",path:"gui.html"},
    {label:"gui_w32",path:"gui_w32.html"},
    {label:"gui_x11",path:"gui_x11.html"}
  ]},

  {label:"Interfaces",children:
  [
    {label:"if_cscop",path:"if_cscop.html"},
    {label:"if_lua",path:"if_lua.html"},
    {label:"if_mzsch",path:"if_mzsch.html"},
    {label:"if_perl",path:"if_perl.html"},
    {label:"if_pyth",path:"if_pyth.html"},
    {label:"if_tcl",path:"if_tcl.html"},
    {label:"if_ole",path:"if_ole.html"},
    {label:"if_ruby",path:"if_ruby.html"},
    {label:"debugger",path:"debugger.html"},
    {label:"workshop",path:"workshop.html"},
    {label:"netbeans",path:"netbeans.html"},
    {label:"sign",path:"sign.html"}
  ]},

  {label:"Versions",children:
  [
    {label:"vi_diff",path:"vi_diff.html"},
    {label:"version4",path:"version4.html"},
    {label:"version5",path:"version5.html"},
    {label:"version6",path:"version6.html"},
    {label:"version7",path:"version7.html"},
    {label:"version8",path:"version8.html"}
  ]},

  {label:"Remarks about specific systems",children:
  [
    {label:"os_390",path:"os_390.html"},
    {label:"os_amiga",path:"os_amiga.html"},
    {label:"os_beos",path:"os_beos.html"},
    {label:"os_dos",path:"os_dos.html"},
    {label:"os_mac",path:"os_mac.html"},
    {label:"os_mint",path:"os_mint.html"},
    {label:"os_msdos",path:"os_msdos.html"},
    {label:"os_os2",path:"os_os2.html"},
    {label:"os_qnx",path:"os_qnx.html"},
    {label:"os_risc",path:"os_risc.html"},
    {label:"os_unix",path:"os_unix.html"},
    {label:"os_vms",path:"os_vms.html"},
    {label:"os_win32",path:"os_win32.html"}
  ]},

  {label:"Standard plugins",children:
  [
    {label:"pi_getscript",path:"pi_getscript.html"},
    {label:"pi_gzip",path:"pi_gzip.html"},
    {label:"pi_logipat",path:"pi_logipat.html"},
    {label:"pi_netrw",path:"pi_netrw.html"},
    {label:"pi_paren",path:"pi_paren.html"},
    {label:"pi_tar",path:"pi_tar.html"},
    {label:"pi_vimball",path:"pi_vimball.html"},
    {label:"pi_zip",path:"pi_zip.html"}
  ]},

  {label:"Filetype plugins",children:
  [
    {label:"pi_spec",path:"pi_spec.html"}
  ]},

  {label:"Others",children:
  [
    {label:"vim_faq",path:"vim_faq.html"}
  ]}
];
